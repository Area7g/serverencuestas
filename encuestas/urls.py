# coding:utf8
from django.conf.urls import patterns, include, url
from encuestas import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    #REST api
    #listamos todas las preguntas
    url(r'^api/questions$', views.indexJSON, name='indexJSON'),
    #listamos las opciones
    url(r'^api/questions/(?P<question_id>\d+)$', views.detailJSON, name='detailJSON'),
    #listamos los votos
    url(r'^api/questions/(?P<question_id>\d+)/vote$', views.voteJSON, name='voteJSON'),
    
    #Cliente web
    # toda expresion regular que contenga un id y es un numero decimal y es al menos uno, por eso '+'
    url(r'^(?P<question_id>\d+)/$', views.detail, name='detail'),
    url(r'^(?P<question_id>\d+)/results/$', views.results, name='results'),
    url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
)
